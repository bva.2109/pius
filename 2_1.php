
<?php
    require '../vendor/autoload.php';
    use Symfony\Component\Validator\Constraints as Assert;
    use Symfony\Component\Validator\Validation;
    use Symfony\Component\Validator\Mapping\ClassMetadata;
    
    class User
    {
        /**
         * @Assert\NotBlank
         */
        public int $id;
        /**
        * @Assert\NotBlank
        * @Assert\Length(
        *      min = 2,
        *      max = 50,
        *      minMessage = "Ваше имя должно быть как минимум {{ limit }} символов",
        *      maxMessage = "Ваше имя не должно быть менeе {{ limit }} символов"
        * )
        */
        public string $name;
        /**
         * @Assert\NotBlank
         */
        public string $email;
        /**
        * @Assert\NotBlank 
        * @Assert\Length(
        *      min = 2,
        *      max = 50,
        *      minMessage = "Ваш пароль должeн быть как минимум {{ limit }} символов",
        *      maxMessage = "Ваш пароль должeн быть менeе {{ limit }} символов"
        * )
        */
        public string $password;
        private $timeInit;
        public function __construct($id,$name,$email,$password)
        {
            $this->id=(int)$id;
            $this->name=$name;
            $this->email=$email;
            $this->password=$password;
            $this->timeInit=time();
        }

        public function getTimeInit(): int
        {
            return $this->timeInit;

        }

        public static function loadValidatorMetadata(ClassMetadata $metadata)
        {
            $metadata->addPropertyConstraint('id', new Assert\NotBlank());
            $metadata->addPropertyConstraint('email', new Assert\NotBlank());
            $metadata->addPropertyConstraint('name', new Assert\NotBlank());
            $metadata->addPropertyConstraint('name', new Assert\Length(array(
                     'min' => 2,
                     'max' => 50,
                     'minMessage' => "Ваше имя должно быть как минимум {{ limit }} символов",
                     'maxMessage' => "Ваше имя не должно быть менeе {{ limit }} символов"
                )
            ));

            $metadata->addPropertyConstraint('password', new Assert\NotBlank());
            $metadata->addPropertyConstraint('password', new Assert\Length(array(
                    'min' => 2,
                    'max' => 50,
                    'minMessage' => "Ваш пароль должeн быть как минимум {{ limit }} символов",
                    'maxMessage' => "Ваш пароль должeн быть менeе {{ limit }} символов"
                )
            ));    
        }

        public function print()
        {
            
            echo "Name: $this->name".'<br>'." Password: $this->password".'<br>'." Id: $this->id".'<br>'." Create date: ".date("d,m,y,H:i",$this->getTimeInit()).'<br>'.'<br>';
        }
    }

    function userConstreints(User $user)
    {
        
        $validator=Validation::createValidatorBuilder()->addMethodMapping('loadValidatorMetadata')->getValidator();

        $violations = $validator->validate($user);
        if (0 !== count($violations)) {
            // there are errors, now you can show them
            foreach ($violations as $violation) {
            echo $violation->getMessage().'<br>';
            }
        }
        else{
            $user->print();
        }
        
    }

    class Comment
    {
        public User $user;
        public string $comment;
        public function __construct(User $user, string $comment)
        {
            $this->user=$user;
            $this->comment=$comment;
        }
        public function print()
        {
            $this->user->print();
            echo 'Comment: '.$this->comment.'<br>'.'<br>';

        }
    }

    echo 'Task 2.1'.'<br>'.'<br>';

    
    userConstreints(new User(12341,'1312','qwert','123'));
    userConstreints(new User(11,'','qwert',''));
    userConstreints(new User(12,'1312','','123'));


    
    echo '<br>'.'Task 2.2'.'<br>'.'<br>';

    $array=[new Comment(new User(123,'vs','cos','1234vv'),'death'),
    new Comment(new User(123,'vs','cos','1234vv'),'death'),
    new Comment(new User(123,'vs','cos','1234vv'),'death')];
    $datetime=isset($_POST['datetime']) ? (int)$_POST['datetime'] : 0;
    echo 'DateTime'.date("d.m.y H:i",$datetime).'<br>';
    foreach($array as $thing)
    {
        if($thing->user->getTimeInit()>$datetime&&$datetime<>0)
        {
            $thing->print();
        }
    }


?>